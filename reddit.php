<?php

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rudolf\OAuth2\Client\Provider\Reddit;

use function Stringy\create as s;

require __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->safeLoad();

$stack = HandlerStack::create();
//$stack->push(Middleware::mapRequest(function (RequestInterface $request) {
//    echo "\n\n\n" . $request->getBody() . "\n\n\n";
//    return $request;
//}));
//
//$stack->push(Middleware::mapResponse(function (ResponseInterface $response) {
//    echo "\n\n\n" . $response->getBody() . "\n\n\n";
//    return $response;
//}));

$httpClient = new Client([
//    'debug' => true,
    'handler' => $stack,
    'timeout' => 15,
]);

$reddit = new Reddit(
    [
        'clientId' => getenv('REDDIT_CLIENT_ID'),
        'clientSecret' => getenv('REDDIT_CLIENT_SECRET'),
        'userAgent' => getenv('REDDIT_CLIENT_USERAGENT'),
        'scopes' => ['submit'],
    ],
    ["httpClient" => $httpClient]
);

echo "Get Reddit Access Token...\n";
$accessToken = $reddit->getAccessToken('password', [
    'username' => getenv('REDDIT_USER_USERNAME'),
    'password' => getenv('REDDIT_USER_PASSWORD'),
]);

echo "Get Arxiv Sanity top papers...\n";
$response = $httpClient->get("http://www.arxiv-sanity.com/top?timefilter=week&vfilter=all");

$matches = [];
preg_match("/var papers = (\[.*\]);/", (string)$response->getBody(), $matches);
$papers = json_decode($matches[1], true);
if (!is_array($papers)) {
    throw new RuntimeException("Failed to parse json from arxiv-sanity!");
}

$top = 3;
echo "Process and post 3 top papers:\n\n";
for ($i = ($top - 1); $i >= 0; $i--) {

    $paper = $papers[$i];
    $paper["title"] = (string)s($paper["title"])
        ->regexReplace('/p{Zl}/', ' ')
        ->collapseWhitespace();

    echo $paper["title"] . "\n" . $paper["link"] . "\n\n";

    $request = $reddit->getAuthenticatedRequest(
        'POST',
        'https://oauth.reddit.com/api/submit',
        $accessToken
    );

    $toMultipart = function (array $postData) {
        $data = [];
        foreach ($postData as $key => $postDatum) {
            $datum = [];
            $datum['name'] = $key;
            $datum['contents'] = $postDatum;
            $data[] = $datum;
        }
        return $data;
    };

    $httpClient->send($request, [
        'multipart' => $toMultipart([
            "title" => $paper["title"],
            "url" => $paper["link"],
            "sr" => getenv('REDDIT_SUBREDDIT'),
            "kind" => "link",
        ]),
    ]);

}